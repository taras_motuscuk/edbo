<?php
 
global $soapClient;
class Soap
{
    private $soapClient;
    private function buildObject($data)
    {
        $res = null;
        if (is_array($data) || is_object($data)) {
            $res = array();
            foreach ($data as $key => $value) {
                if (is_string($key)) {
                    switch ($key) {
                        case "string":
                            $res[] = $value;
                            break;
                        case "int":
                            $res[] = $value;
                            break;
                        case "any": //тут xml
                            $res[] = simplexml_load_string("<?xml version=\"1.0\"?><document>" . $value .
                                "</document>");
                            break;
                        default:
                            $res[] = $this->buildObject($value);
                            break;
                    }
                }
                if (is_integer($key)) {
                    $res[] = $this->buildObject($value);
                }
            }
        } else {
            $res = $data;
        }
        //if(is_array($res) && count($res) == 1) $res = $res[0];
        if (is_array($res) && (count($res) == 1)) {
            foreach ($res as $key => $value) {
            }
            $res = $res[$key];
        }
        return $res;
    }
    public function invoke($method, $params)
    {
        try {
            $invs = $this->soapClient->__soapCall($method, array($params));
            $sresult = $invs->{$method . "Result"};
            $res = $this->buildObject($sresult);
            return $res;
        }
        catch (exception $e) {
            print "Exception in try call method $method<br>\n";
            print "Parameters:<br>\n";
            print_r($params);
            var_dump($sresult);
            print "<br>\n";
            throw $e;
        }
    }
    public function __construct()
    {
        global $soapClient;
        $this->soapClient = $soapClient;
    }
}
global $soap;
function _edboLogin($hostaddress)
{
    if ($hostaddress == 'person')
        $soapAddress = variable_get('edbo_variable_server') . variable_get('edbo_variable_soap_person');
    if ($hostaddress == 'guides')
        $soapAddress = variable_get('edbo_variable_server') . variable_get('edbo_variable_soap_guides');
    global $soapClient;
    $soapClient = new SoapClient($soapAddress, array('encoding' => 'utf-8'));
    global $soap;
    $soap = new Soap();
     $sessionId =    $soap->invoke("Login",
    array(
    "User" =>  variable_get('edbo_variable_login'), 
    "Password" => variable_get('edbo_variable_pass'), 
    "ClearPreviewSession" => 1, 
    "ApplicationKey" =>variable_get('edbo_variable_appkey'))); 
    return $sessionId;

}
function _edboPersonsStudentsGet($SessionGUID)
{
    global $soapClient;
    global $soap;
    db_query("TRUNCATE PersonsStudentsGet");
    $PersonsStudentsGet = $soap->invoke("PersonsStudentsGet", array(
        "SessionGUID" => $SessionGUID,
        "ActualDate" => "",
        "Id_Language" => 1,
        "UniversityKode" => "62c34f4d-0a7c-4e69-9eb2-522c069121d1",
         "Filters" => "",
        ));
    $cnt = count($PersonsStudentsGet);
    foreach ($PersonsStudentsGet as $pr) {
        $pr[] = 2712;
        $text = implode('\',\'', $pr);
        $insert_query = "INSERT INTO `nmc`.`PersonsStudentsGet` (`Id_Person`, `Resident`, 
        `PersonCodeU`, `Birthday`, `DateLastChange`, `LastName`, 
        `FirstName`, `MiddleName`, `Id_PersonSex`, `PersonSexName`, 
        `Id_PersonTypeDict`, `PersonTypeName`, `Id_UniversityGroup`, 
        `Id_PersonEducationHistoryType`, `Id_PersonEducationPaymentType`, 
        `PersonEducationHistoryDesciption`, `Id_AcademicYear`, `Id_Course`,
         `Id_PersonEducationForm`, `Id_Qualification`, `SpecCode`, `UniversityFacultetKode`,
          `UniversityGroupFullName`, `UniversityGroupShortName`, `UniversityGroupDescription`,
           `QualificationName`, `PersonEducationFormName`, `AcademicYearName`, `AcademicYearDescription`, 
           `AcademicYearIsActive`, `CourseName`, `SpecClasifierCode`, `UniversityFacultetFullName`,
            `UniversityFacultetShortName`, `PersonEducationHistoryTypeName`, 
            `PersonEducationHistoryTypeDescription`, `PersonEducationPaymentTypeName`,`vnz`) VALUES ('" .
            $text . "');";
        $res = db_query($insert_query);
    }
    $PersonsStudentsGet = $soap->invoke("PersonsStudentsGet", array(
        "SessionGUID" => $SessionGUID,
        "ActualDate" => "",
        "Id_Language" => 1,
        "UniversityKode" => "ac189d29-eec1-4aaf-8ace-d952dff91344",
        "Filters" => "",
        ));
    foreach ($PersonsStudentsGet as $pr) {
        $pr[] = 341;
        $text = implode('\',\'', $pr);
        $insert_query = "INSERT INTO `nmc`.`PersonsStudentsGet` (`Id_Person`, `Resident`, 
        `PersonCodeU`, `Birthday`, `DateLastChange`, `LastName`, 
        `FirstName`, `MiddleName`, `Id_PersonSex`, `PersonSexName`, 
        `Id_PersonTypeDict`, `PersonTypeName`, `Id_UniversityGroup`, 
        `Id_PersonEducationHistoryType`, `Id_PersonEducationPaymentType`, 
        `PersonEducationHistoryDesciption`, `Id_AcademicYear`, `Id_Course`,
         `Id_PersonEducationForm`, `Id_Qualification`, `SpecCode`, `UniversityFacultetKode`,
          `UniversityGroupFullName`, `UniversityGroupShortName`, `UniversityGroupDescription`,
           `QualificationName`, `PersonEducationFormName`, `AcademicYearName`, `AcademicYearDescription`, 
           `AcademicYearIsActive`, `CourseName`, `SpecClasifierCode`, `UniversityFacultetFullName`,
            `UniversityFacultetShortName`, `PersonEducationHistoryTypeName`, 
            `PersonEducationHistoryTypeDescription`, `PersonEducationPaymentTypeName`,`vnz`) VALUES ('" .
            $text . "');";
        $res = db_query($insert_query);
    }
    $PersonsStudentsGet = $soap->invoke("PersonsStudentsGet", array(
        "SessionGUID" => $SessionGUID,
        "ActualDate" => "",
        "Id_Language" => 1,
         "UniversityKode" => "6782958c-8b1b-4633-bc4b-eb45cef3fdf5",
        "Filters" => "",
        ));
    foreach ($PersonsStudentsGet as $pr) {
        $pr[] = 2736;
        $text = implode('\',\'', $pr);
        $insert_query = "INSERT INTO `nmc`.`PersonsStudentsGet` (`Id_Person`, `Resident`, 
        `PersonCodeU`, `Birthday`, `DateLastChange`, `LastName`, 
        `FirstName`, `MiddleName`, `Id_PersonSex`, `PersonSexName`, 
        `Id_PersonTypeDict`, `PersonTypeName`, `Id_UniversityGroup`, 
        `Id_PersonEducationHistoryType`, `Id_PersonEducationPaymentType`, 
        `PersonEducationHistoryDesciption`, `Id_AcademicYear`, `Id_Course`,
         `Id_PersonEducationForm`, `Id_Qualification`, `SpecCode`, `UniversityFacultetKode`,
          `UniversityGroupFullName`, `UniversityGroupShortName`, `UniversityGroupDescription`,
           `QualificationName`, `PersonEducationFormName`, `AcademicYearName`, `AcademicYearDescription`, 
           `AcademicYearIsActive`, `CourseName`, `SpecClasifierCode`, `UniversityFacultetFullName`,
            `UniversityFacultetShortName`, `PersonEducationHistoryTypeName`, 
            `PersonEducationHistoryTypeDescription`, `PersonEducationPaymentTypeName`,`vnz`) VALUES ('" .
            $text . "');";
        $res = db_query($insert_query);
    }
    $PersonsStudentsGet = $soap->invoke("PersonsStudentsGet", array(
        "SessionGUID" => $SessionGUID,
        "ActualDate" => "16.03.2015 16:25:00",
        "Id_Language" => 1,
        //"UniversityKode"=>"62c34f4d-0a7c-4e69-9eb2-522c069121d1",
        "UniversityKode" => "99a1817e-7a91-4cc0-b931-b410b732eb69",
        "Filters" => "",
        ));
    foreach ($PersonsStudentsGet as $pr) {
        $pr[] = 2738;
        $text = implode('\',\'', $pr);
        $insert_query = "INSERT INTO `nmc`.`PersonsStudentsGet` (`Id_Person`, `Resident`, 
        `PersonCodeU`, `Birthday`, `DateLastChange`, `LastName`, 
        `FirstName`, `MiddleName`, `Id_PersonSex`, `PersonSexName`, 
        `Id_PersonTypeDict`, `PersonTypeName`, `Id_UniversityGroup`, 
        `Id_PersonEducationHistoryType`, `Id_PersonEducationPaymentType`, 
        `PersonEducationHistoryDesciption`, `Id_AcademicYear`, `Id_Course`,
         `Id_PersonEducationForm`, `Id_Qualification`, `SpecCode`, `UniversityFacultetKode`,
          `UniversityGroupFullName`, `UniversityGroupShortName`, `UniversityGroupDescription`,
           `QualificationName`, `PersonEducationFormName`, `AcademicYearName`, `AcademicYearDescription`, 
           `AcademicYearIsActive`, `CourseName`, `SpecClasifierCode`, `UniversityFacultetFullName`,
            `UniversityFacultetShortName`, `PersonEducationHistoryTypeName`, 
            `PersonEducationHistoryTypeDescription`, `PersonEducationPaymentTypeName`,`vnz`) VALUES ('" .
            $text . "');";
        $res = db_query($insert_query);
    }
    $PersonsStudentsGet = $soap->invoke("PersonsStudentsGet", array(
        "SessionGUID" => $SessionGUID,
        "ActualDate" => "16.03.2015 16:25:00",
        "Id_Language" => 1,
        //"UniversityKode"=>"62c34f4d-0a7c-4e69-9eb2-522c069121d1",
        "UniversityKode" => "bf343633-253a-46c8-9017-9ea65eab0dd4",
        "Filters" => "",
        ));
    foreach ($PersonsStudentsGet as $pr) {
        $pr[] = 2739;
        $text = implode('\',\'', $pr);
        $insert_query = "INSERT INTO `nmc`.`PersonsStudentsGet` (`Id_Person`, `Resident`, 
        `PersonCodeU`, `Birthday`, `DateLastChange`, `LastName`, 
        `FirstName`, `MiddleName`, `Id_PersonSex`, `PersonSexName`, 
        `Id_PersonTypeDict`, `PersonTypeName`, `Id_UniversityGroup`, 
        `Id_PersonEducationHistoryType`, `Id_PersonEducationPaymentType`, 
        `PersonEducationHistoryDesciption`, `Id_AcademicYear`, `Id_Course`,
         `Id_PersonEducationForm`, `Id_Qualification`, `SpecCode`, `UniversityFacultetKode`,
          `UniversityGroupFullName`, `UniversityGroupShortName`, `UniversityGroupDescription`,
           `QualificationName`, `PersonEducationFormName`, `AcademicYearName`, `AcademicYearDescription`, 
           `AcademicYearIsActive`, `CourseName`, `SpecClasifierCode`, `UniversityFacultetFullName`,
            `UniversityFacultetShortName`, `PersonEducationHistoryTypeName`, 
            `PersonEducationHistoryTypeDescription`, `PersonEducationPaymentTypeName`,`vnz`) VALUES ('" .
            $text . "');";
        $res = db_query($insert_query);
    }
    $PersonsStudentsGet = $soap->invoke("PersonsStudentsGet", array(
        "SessionGUID" => $SessionGUID,
        "ActualDate" => "16.03.2015 16:25:00",
        "Id_Language" => 1,
        //"UniversityKode"=>"62c34f4d-0a7c-4e69-9eb2-522c069121d1",
        "UniversityKode" => "bd9a3c6b-0795-41d9-9432-04b3e60ac7a6",
        "Filters" => "",
        ));
    foreach ($PersonsStudentsGet as $pr) {
        $pr[] = 2740;
        $text = implode('\',\'', $pr);
        $insert_query = "INSERT INTO `nmc`.`PersonsStudentsGet` (`Id_Person`, `Resident`, 
        `PersonCodeU`, `Birthday`, `DateLastChange`, `LastName`, 
        `FirstName`, `MiddleName`, `Id_PersonSex`, `PersonSexName`, 
        `Id_PersonTypeDict`, `PersonTypeName`, `Id_UniversityGroup`, 
        `Id_PersonEducationHistoryType`, `Id_PersonEducationPaymentType`, 
        `PersonEducationHistoryDesciption`, `Id_AcademicYear`, `Id_Course`,
         `Id_PersonEducationForm`, `Id_Qualification`, `SpecCode`, `UniversityFacultetKode`,
          `UniversityGroupFullName`, `UniversityGroupShortName`, `UniversityGroupDescription`,
           `QualificationName`, `PersonEducationFormName`, `AcademicYearName`, `AcademicYearDescription`, 
           `AcademicYearIsActive`, `CourseName`, `SpecClasifierCode`, `UniversityFacultetFullName`,
            `UniversityFacultetShortName`, `PersonEducationHistoryTypeName`, 
            `PersonEducationHistoryTypeDescription`, `PersonEducationPaymentTypeName`,`vnz`) VALUES ('" .
            $text . "');";
        $res = db_query($insert_query);
    }
    //print_r($PersonsStudentsGet);
    //exit;
    return ($cnt + count($PersonsStudentsGet));
}
function _edboPersonsStudentsGrupsPersonsAdditionalGet2($SessionGUID)
{
    global $soapClient;
    global $soap;
    $PersonsStudentsGet = $soap->invoke("PersonsStudentsGrupsPersonsAdditionalGet2",
        array(
        "SessionGUID" => $SessionGUID,
        "ActualDate" => "16.03.2015 16:25:00",
        "Id_Language" => 1,
        //"UniversityKode"=>"62c34f4d-0a7c-4e69-9eb2-522c069121d1",
        "UniversityKode" => "ac189d29-eec1-4aaf-8ace-d952dff91344",
        "Filters" => "",
        ));
    echo $SessionGUID;
     
     return $PersonsStudentsGet;
}
function _edboQualificationGroupsGet($SessionGUID)
{
    global $soapClient;
    global $soap;
    $PersonsStudentsGet = $soap->invoke("QualificationGroupsGet", array(
        "SessionGUID" => $SessionGUID,
        "ActualDate" => "",
        "Id_Language" => 1,
         
        ));
    echo $SessionGUID;
    $pg = $soap->invoke("EducationQualificationsAdd", array(
        "SessionGUID" => $SessionGUID,
         
        ));
    $error = $soap->invoke("GetLastError", array("SessionGUID" => $SessionGUID));

     
    return $PersonsStudentsGet;
}


function _edboPersonEducationsGet($SessionGUID)
{
    global $soapClient;
    global $soap;
    set_time_limit(0);
     db_truncate('PersonEducationsGet2')->execute();
     $nodes = db_query("SELECT distinct PersonCodeU FROM {PersonsStudentsGet}");
    foreach ($nodes as $node) {
        $PersonCodeU = $node->PersonCodeU; //}
        $PersonsStudentsEdGet = $soap->invoke("PersonEducationsGet2", array(
            "SessionGUID" => $SessionGUID,
            "ActualDate" => date("d.m.Y H:i:s", time()),
            "Id_Language" => 1,
            //"UniversityKode"=>"62c34f4d-0a7c-4e69-9eb2-522c069121d1",
            "PersonCodeU" => $PersonCodeU,
            "Id_PersonEducation" => 0,
            "Id_PersonEducationType" => 3,
            "Filters" => "",
            ));
        $education = array();
        if (empty($PersonsStudentsEdGet[0][0]))
            $education[0] = $PersonsStudentsEdGet;
        else
            $education = $PersonsStudentsEdGet;
        foreach ($education as $PersonsStudentsGet)
            $id = db_insert('PersonEducationsGet2')->fields(array(
                'Id_PersonEducation' => $PersonsStudentsGet[0],
                'Id_PersonEducationForm' => $PersonsStudentsGet[1],
                'PersonEducationFormName' => $PersonsStudentsGet[2],
                'Id_PersonEducationType' => $PersonsStudentsGet[3],
                'PersonEducationDateBegin' => $PersonsStudentsGet[4],
                'PersonEducationDateEnd' => $PersonsStudentsGet[5],
                'InstitutionCode' => $PersonsStudentsGet[6],
                'Id_University' => $PersonsStudentsGet[7],
                'UniversityFullName' => $PersonsStudentsGet[8],
                'UniversityFacultetKode' => $PersonsStudentsGet[9],
                'UniversityFacultetFullName' => $PersonsStudentsGet[10],
                'QualificationName' => $PersonsStudentsGet[11],
                'SpecDirectionName' => $PersonsStudentsGet[12],
                'SpecSpecialityName' => $PersonsStudentsGet[13],
                'SpecScecializationName' => $PersonsStudentsGet[14],
                'SpecClasifierCode' => $PersonsStudentsGet[15],
                'SpecSpecialityClasifierCode' => $PersonsStudentsGet[16],
                'PersonCodeU' => $PersonsStudentsGet[17],
                'CourseName' => $PersonsStudentsGet[18],
                'AcademicYearName' => $PersonsStudentsGet[19],
                'Id_PersonEducationHistoryOrders' => $PersonsStudentsGet[20],
                'PersonEducationHistoryOrdersNumber' => $PersonsStudentsGet[21],
                'PersonEducationHistoryOrdersDate' => $PersonsStudentsGet[22],
                'UniversityGroupFullName' => $PersonsStudentsGet[23],
                'PersonEducationPaymentTypeName' => $PersonsStudentsGet[24],
                'PersonEducationHistoryTypeName' => $PersonsStudentsGet[25],
                'IsRefill' => $PersonsStudentsGet[26],
                'Id_PersonRequest' => $PersonsStudentsGet[27],
                'Id_PersonDocument' => $PersonsStudentsGet[28],
                'DocumentSeries' => $PersonsStudentsGet[29],
                'DocumentNumbers' => $PersonsStudentsGet[30],
                'DocumentDateGet' => $PersonsStudentsGet[31],
                'Id_PersonDocumentType' => $PersonsStudentsGet[32],
                'PersonDocumentTypeName' => $PersonsStudentsGet[33],
                'Id_PersonEducationPaymentType' => $PersonsStudentsGet[34],
                'Id_PersonEducationHistoryType' => $PersonsStudentsGet[35],
                'Id_AcademicYear' => $PersonsStudentsGet[36],
                'Id_Course' => $PersonsStudentsGet[37],
                'Id_Qualification' => $PersonsStudentsGet[38],
                'Id_QualificationGroup' => $PersonsStudentsGet[39],
                'Id_UniversityGroup' => $PersonsStudentsGet[40],
                'SpecCode' => $PersonsStudentsGet[41],
                'IsSecondHigher' => $PersonsStudentsGet[42],
                'SpecProfessionClassifierCode1' => $PersonsStudentsGet[43],
                'SpecProfessionName1' => $PersonsStudentsGet[44],
                'SpecProfessionClassifierCode2' => $PersonsStudentsGet[45],
                'SpecProfessionName2' => $PersonsStudentsGet[46],
                'SpecProfessionClassifierCode3' => $PersonsStudentsGet[47],
                'SpecProfessionName3' => $PersonsStudentsGet[48],
                'SpecProfessionClassifierCode4' => $PersonsStudentsGet[49],
                'SpecProfessionName4' => $PersonsStudentsGet[50],
                'SpecProfessionClassifierCode5' => $PersonsStudentsGet[51],
                'SpecProfessionName5' => $PersonsStudentsGet[52],
                'SpecProfession1_MaxRang' => $PersonsStudentsGet[53],
                'SpecProfession2_MaxRang' => $PersonsStudentsGet[54],
                'SpecProfession3_MaxRang' => $PersonsStudentsGet[55],
                'SpecProfession4_MaxRang' => $PersonsStudentsGet[56],
                'SpecProfession5_MaxRang' => $PersonsStudentsGet[57],
                'SpecProfession1_CurRang' => $PersonsStudentsGet[58],
                'SpecProfession2_CurRang' => $PersonsStudentsGet[59],
                'SpecProfession3_CurRang' => $PersonsStudentsGet[60],
                'SpecProfession4_CurRang' => $PersonsStudentsGet[61],
                'SpecProfession5_CurRang' => $PersonsStudentsGet[62],
                'Id_SpecProfessions1RangType' => $PersonsStudentsGet[63],
                'Id_SpecProfessions2RangType' => $PersonsStudentsGet[64],
                'Id_SpecProfessions3RangType' => $PersonsStudentsGet[65],
                'Id_SpecProfessions4RangType' => $PersonsStudentsGet[66],
                'Id_SpecProfessions5RangType' => $PersonsStudentsGet[67],
                'SpecProfessionsRangTypeName1' => $PersonsStudentsGet[68],
                'SpecProfessionsRangTypeName2' => $PersonsStudentsGet[69],
                'SpecProfessionsRangTypeName3' => $PersonsStudentsGet[70],
                'SpecProfessionsRangTypeName4' => $PersonsStudentsGet[71],
                'SpecProfessionsRangTypeName5' => $PersonsStudentsGet[72],
                'IsAfterDiplomEducation' => $PersonsStudentsGet[73],
                'BudjetYear' => $PersonsStudentsGet[74],
                'IsProgramDoubleDiploms' => $PersonsStudentsGet[75],
                ))->execute();
    }
      return $id;
}
function _edboDayForm($SessionGUID)
{
    global $soapClient;
    global $soap;
    $rows = array();
    $query = db_query("SELECT distinct `PersonCodeU`,`FIO`,`Id_PersonRequest` FROM `PersonRequestsGet36` WHERE `PersonEducationFormName`='Денна' AND `PersonRequestStatusTypeName`='До наказу' ");
    foreach ($query as $qu) {
        $pku = $qu->PersonCodeU;
        $education = $soap->invoke("PersonEducationsGet2", array(
            "SessionGUID" => $SessionGUID,
            "ActualDate" => " ",
            "Id_Language" => 1,
            "PersonCodeU" => $pku,
            "Id_PersonEducation" => 0,
            "Id_PersonEducationType" => 3,
            "Filters" => "",
            ));
        if (!is_array($education[0]))
            $rows[] = array_merge(array($qu->Id_PersonRequest, $qu->FIO), $education);
        else
            foreach ($education as $ed) {
                $rows[] = array_merge(array($qu->Id_PersonRequest, $qu->FIO), $ed);
            }
    }
    module_load_include('inc', 'phpexcel');
    $headers_xls['Data'] = array(
        'id_request',
        'fio',
        'Id_PersonEducation',
        'Id_PersonEducationForm',
        'PersonEducationFormName',
        'Id_PersonEducationType',
        'PersonEducationDateBegin',
        'PersonEducationDateEnd',
        'InstitutionCode',
        'Id_University',
        'UniversityFullName',
        'UniversityFacultetKode',
        'UniversityFacultetFullName',
        'QualificationName',
        'SpecDirectionName',
        'SpecSpecialityName',
        'SpecScecializationName',
        'SpecClasifierCode',
        'SpecSpecialityClasifierCode',
        'PersonCodeU',
        'CourseName',
        'AcademicYearName',
        'Id_PersonEducationHistoryOrders',
        'PersonEducationHistoryOrdersNumber',
        'PersonEducationHistoryOrdersDate',
        'UniversityGroupFullName',
        'PersonEducationPaymentTypeName',
        'PersonEducationHistoryTypeName',
        'IsRefill',
        'Id_PersonRequest',
        'Id_PersonDocument',
        'DocumentSeries',
        'DocumentNumbers',
        'DocumentDateGet',
        'Id_PersonDocumentType',
        'PersonDocumentTypeName',
        'Id_PersonEducationPaymentType',
        'Id_PersonEducationHistoryType',
        'Id_AcademicYear',
        'Id_Course',
        'Id_Qualification',
        'Id_QualificationGroup',
        'Id_UniversityGroup',
        'SpecCode',
        'IsSecondHigher',
        'SpecProfessionClassifierCode1',
        'SpecProfessionName1',
        'SpecProfessionClassifierCode2',
        'SpecProfessionName2',
        'SpecProfessionClassifierCode3',
        'SpecProfessionName3',
        'SpecProfessionClassifierCode4',
        'SpecProfessionName4',
        'SpecProfessionClassifierCode5',
        'SpecProfessionName5',
        'SpecProfession1_MaxRang',
        'SpecProfession2_MaxRang',
        'SpecProfession3_MaxRang',
        'SpecProfession4_MaxRang',
        'SpecProfession5_MaxRang',
        'SpecProfession1_CurRang',
        'SpecProfession2_CurRang',
        'SpecProfession3_CurRang',
        'SpecProfession4_CurRang',
        'SpecProfession5_CurRang',
        'Id_SpecProfessions1RangType',
        'Id_SpecProfessions2RangType',
        'Id_SpecProfessions3RangType',
        'Id_SpecProfessions4RangType',
        'Id_SpecProfessions5RangType',
        'SpecProfessionsRangTypeName1',
        'SpecProfessionsRangTypeName2',
        'SpecProfessionsRangTypeName3',
        'SpecProfessionsRangTypeName4',
        'SpecProfessionsRangTypeName5',
        'IsAfterDiplomEducation',
        'BudjetYear',
        'IsProgramDoubleDiploms');
    $headers_xls['Error'] = $headers_xls['Data'];
    $data_xls['Data'] = $rows;
    foreach ($rows as $row) {
        if ($row[27] != 'Відрахування' && $row[27] != 'Закінчення навчання' && $row[27] !=
            'Скасування' && $row[4] == 'Денна')
            $data_xls['Error'][] = $row;
    }
    $dir = file_stream_wrapper_get_instance_by_uri('public://')->realpath();
    $filename = 'zvit_2016.xls';
    $path = "$dir/$filename";
    // Use the .xls format
    $options = array('format' => 'xls');
    $result = phpexcel_export($headers_xls, $data_xls, $path, $options);
    //var_dump($education);

}
function _edboPersonRequestsGet3($SessionGUID)
{
    global $soapClient;
    global $soap;
    ini_set("max_execution_time", "0");

    $result = db_query("SELECT DISTINCT  `IdPersonRequest` FROM  `PersonRequestIdsGet6` ");
    db_truncate('PersonRequestsGet36')->execute();
    foreach ($result as $record) {
        $idpr = $record->IdPersonRequest;
        $PersonRequestsGet3 = $soap->invoke("PersonRequestsGet3", array(
            "SessionGUID" => $SessionGUID,
            "ActualDate" => "",
            "Id_Language" => 1,
            "PersonCodeU" => "",
            "Id_PersonRequestSeasons" => 6,
            "Id_PersonRequest" => $idpr,
            "UniversityFacultetKode" => "",
            "Id_PersonEducationForm" => "",
            "Id_Qualification" => 0,
            "Filters" => ""));
         $insertstr = "INSERT INTO `nmc`.`PersonRequestsGet36` (`Id_PersonRequest`, `Id_PersonRequestSeasons`, `PersonCodeU`, `UniversitySpecialitiesKode`, `NameRequestSeason`, `RequestPerPerson`, `Id_UniversitySpecialities`, `UniversitySpecialitiesDateBegin`, `UniversitySpecialitiesDateEnd`, `UniversityKode`, `UniversityFullName`, `UniversityShortName`, `UniversityFacultetKode`, `UniversityFacultetFullName`, `UniversityFacultetShortName`, `SpecCode`, `SpecClasifierCode`, `SpecIndastryName`, `SpecDirectionName`, `SpecSpecialityName`, `Id_Language`, `SpecScecializationCode`, `SpecScecializationName`, `OriginalDocumentsAdd`, `Id_PersonRequestStatus`, `Id_PersonRequestStatusType`, `PersonRequestStatusCode`, `Id_PersonRequestStatusTypeName`, `PersonRequestStatusTypeName`, `Descryption`, `IsNeedHostel`, `CodeOfBusiness`, `Id_PersonEnteranceTypes`, `PersonEnteranceTypeName`, `Id_PersonRequestExaminationCause`, `PersonRequestExaminationCauseName`, `IsContract`, `IsBudget`, `Id_PersonEducationForm`, `PersonEducationFormName`, `KonkursValue`, `KonkursValueSource`, `PriorityRequest`, `KonkursValueCorrectValue`, `KonkursValueCorrectValueDescription`, `Id_PersonRequestSeasonDetails`, `Id_Qualification`, `QualificationName`, `Id_PersonDocumentType`, `PersonDocumentTypeName`, `Id_PersonDocument`, `EntrantDocumentSeries`, `EntrantDocumentNumbers`, `EntrantDocumentDateGet`, `EntrantDocumentIssued`, `EntrantDocumentValue`, `IsCheckForPaperCopy`, `IsNotCheckAttestat`, `IsForeinghEntrantDocumet`, `RequestEnteranseCodes`, `Id_UniversityEntrantWave`, `RequestStatusIsBudejt`, `RequestStatusIsContract`, `UniversityEntrantWaveName`, `IsHigherEducation`, `SkipDocumentValue`, `Id_PersonDocumentsAwardType`, `PersonDocumentsAwardTypeName`, `Id_OrderOfEnrollment`, `SpecSpecialityClasifierCode`, `Id_PersonName`, `FIO`, `UniversityPhone`, `DateCreate`, `StatusDateSet`, `BossName`, `Adress`, `Email`, `Phone`, `WebSite`, `BossNameW`, `AdressW`, `EmailW`, `PhoneW`, `WebSiteW`, `IsEz`, `DateRegistration`, `AdressDocumentGetW`, `PersonRequestStatusTypeNameEz`, `RequestPriority`) VALUES (";
        foreach ($PersonRequestsGet3 as $val) {
            $insertstr .= "'" . mysql_real_escape_string($val) . "',";
        }
        $insertstr = substr($insertstr, 0, -1);
        $insertstr .= ")";

        $res = db_query($insertstr);


    }

}
function _edboPersonRequestsIdsGet($SessionGUID)
{
    global $soapClient;
    global $soap;
    $PersonRequestsIdset = $soap->invoke("PersonRequestsIdsGet", array(
        "SessionGUID" => $SessionGUID,
        "Id_Language" => 1,
        "Id_PersonRequestSeasons" => 6,
        "UniversityKode" => "ac189d29-eec1-4aaf-8ace-d952dff91344"));
    $PersonRequestsIdset2 = $soap->invoke("PersonRequestsIdsGet", array(
        "SessionGUID" => $SessionGUID,
        "Id_Language" => 1,
        "Id_PersonRequestSeasons" => 6,
        "UniversityKode" => "62c34f4d-0a7c-4e69-9eb2-522c069121d1"));

    db_truncate('PersonRequestIdsGet6')->execute();
    $ii = 0;
    foreach ($PersonRequestsIdset as $PersonRequestsId) {
        $ii++;
        $id = db_insert('PersonRequestIdsGet6')->fields(array(
            'IdPersonRequest' => $PersonRequestsId[0],
            'PersonCodeU' => $PersonRequestsId[1],
            'DateLastChange' => $PersonRequestsId[2],

            ))->execute();
    }
    foreach ($PersonRequestsIdset2 as $PersonRequestsId1) {
        $ii++;
        $id = db_insert('PersonRequestIdsGet6')->fields(array(
            'IdPersonRequest' => $PersonRequestsId1[0],
            'PersonCodeU' => $PersonRequestsId1[1],
            'DateLastChange' => $PersonRequestsId1[2],

            ))->execute();
    }
    //print_r($PersonRequestsIdset[0]);
    return $ii;

}
function _edboPersonEducationsGet5($SessionGUID)
{
    global $soapClient;
    global $soap;
    set_time_limit(0);
    $path = 'import.xlsx';
     module_load_include('inc', 'phpexcel');
    $result = phpexcel_import($path);
     db_truncate('PersonEducationsGet5')->execute();
      foreach ($result[0] as $person_doc) {
        $PersonCodeUGet = $soap->invoke("PersonGetId", array(
            "SessionGUID" => $SessionGUID,
            "Id_Person" => $person_doc['np'],
            "PersonCodeU" => "",
            ));

        $PersonCodeU = $PersonCodeUGet[1]; //}
      
        $PersonsStudentsEdGet = $soap->invoke("PersonEducationsGet2", array(
            "SessionGUID" => $SessionGUID,
            "ActualDate" => date("d.m.Y H:i:s", time()),
            "Id_Language" => 1,
             "PersonCodeU" => $PersonCodeU,
            "Id_PersonEducation" => 0,
            "Id_PersonEducationType" => 3,
            "Filters" => "",
            ));
        $education = array();
        if (empty($PersonsStudentsEdGet[0][0]))
            $education[0] = $PersonsStudentsEdGet;
        else
            $education = $PersonsStudentsEdGet;
        foreach ($education as $PersonsStudentsGet)
            $id = db_insert('PersonEducationsGet5')->fields(array(
                'pib' => $person_doc['pib'],
                'Id_PersonEducation' => $PersonsStudentsGet[0],
                'Id_PersonEducationForm' => $PersonsStudentsGet[1],
                'PersonEducationFormName' => $PersonsStudentsGet[2],
                'Id_PersonEducationType' => $PersonsStudentsGet[3],
                'PersonEducationDateBegin' => $PersonsStudentsGet[4],
                'PersonEducationDateEnd' => $PersonsStudentsGet[5],
                'InstitutionCode' => $PersonsStudentsGet[6],
                'Id_University' => $PersonsStudentsGet[7],
                'UniversityFullName' => $PersonsStudentsGet[8],
                'UniversityFacultetKode' => $PersonsStudentsGet[9],
                'UniversityFacultetFullName' => $PersonsStudentsGet[10],
                'QualificationName' => $PersonsStudentsGet[11],
                'SpecDirectionName' => $PersonsStudentsGet[12],
                'SpecSpecialityName' => $PersonsStudentsGet[13],
                'SpecScecializationName' => $PersonsStudentsGet[14],
                'SpecClasifierCode' => $PersonsStudentsGet[15],
                'SpecSpecialityClasifierCode' => $PersonsStudentsGet[16],
                'PersonCodeU' => $PersonsStudentsGet[17],
                'CourseName' => $PersonsStudentsGet[18],
                'AcademicYearName' => $PersonsStudentsGet[19],
                'Id_PersonEducationHistoryOrders' => $PersonsStudentsGet[20],
                'PersonEducationHistoryOrdersNumber' => $PersonsStudentsGet[21],
                'PersonEducationHistoryOrdersDate' => $PersonsStudentsGet[22],
                'UniversityGroupFullName' => $PersonsStudentsGet[23],
                'PersonEducationPaymentTypeName' => $PersonsStudentsGet[24],
                'PersonEducationHistoryTypeName' => $PersonsStudentsGet[25],
                'IsRefill' => $PersonsStudentsGet[26],
                'Id_PersonRequest' => $PersonsStudentsGet[27],
                'Id_PersonDocument' => $PersonsStudentsGet[28],
                'DocumentSeries' => $PersonsStudentsGet[29],
                'DocumentNumbers' => $PersonsStudentsGet[30],
                'DocumentDateGet' => $PersonsStudentsGet[31],
                'Id_PersonDocumentType' => $PersonsStudentsGet[32],
                'PersonDocumentTypeName' => $PersonsStudentsGet[33],
                'Id_PersonEducationPaymentType' => $PersonsStudentsGet[34],
                'Id_PersonEducationHistoryType' => $PersonsStudentsGet[35],
                'Id_AcademicYear' => $PersonsStudentsGet[36],
                'Id_Course' => $PersonsStudentsGet[37],
                'Id_Qualification' => $PersonsStudentsGet[38],
                'Id_QualificationGroup' => $PersonsStudentsGet[39],
                'Id_UniversityGroup' => $PersonsStudentsGet[40],
                'SpecCode' => $PersonsStudentsGet[41],
                'IsSecondHigher' => $PersonsStudentsGet[42],
                'SpecProfessionClassifierCode1' => $PersonsStudentsGet[43],
                'SpecProfessionName1' => $PersonsStudentsGet[44],
                'SpecProfessionClassifierCode2' => $PersonsStudentsGet[45],
                'SpecProfessionName2' => $PersonsStudentsGet[46],
                'SpecProfessionClassifierCode3' => $PersonsStudentsGet[47],
                'SpecProfessionName3' => $PersonsStudentsGet[48],
                'SpecProfessionClassifierCode4' => $PersonsStudentsGet[49],
                'SpecProfessionName4' => $PersonsStudentsGet[50],
                'SpecProfessionClassifierCode5' => $PersonsStudentsGet[51],
                'SpecProfessionName5' => $PersonsStudentsGet[52],
                'SpecProfession1_MaxRang' => $PersonsStudentsGet[53],
                'SpecProfession2_MaxRang' => $PersonsStudentsGet[54],
                'SpecProfession3_MaxRang' => $PersonsStudentsGet[55],
                'SpecProfession4_MaxRang' => $PersonsStudentsGet[56],
                'SpecProfession5_MaxRang' => $PersonsStudentsGet[57],
                'SpecProfession1_CurRang' => $PersonsStudentsGet[58],
                'SpecProfession2_CurRang' => $PersonsStudentsGet[59],
                'SpecProfession3_CurRang' => $PersonsStudentsGet[60],
                'SpecProfession4_CurRang' => $PersonsStudentsGet[61],
                'SpecProfession5_CurRang' => $PersonsStudentsGet[62],
                'Id_SpecProfessions1RangType' => $PersonsStudentsGet[63],
                'Id_SpecProfessions2RangType' => $PersonsStudentsGet[64],
                'Id_SpecProfessions3RangType' => $PersonsStudentsGet[65],
                'Id_SpecProfessions4RangType' => $PersonsStudentsGet[66],
                'Id_SpecProfessions5RangType' => $PersonsStudentsGet[67],
                'SpecProfessionsRangTypeName1' => $PersonsStudentsGet[68],
                'SpecProfessionsRangTypeName2' => $PersonsStudentsGet[69],
                'SpecProfessionsRangTypeName3' => $PersonsStudentsGet[70],
                'SpecProfessionsRangTypeName4' => $PersonsStudentsGet[71],
                'SpecProfessionsRangTypeName5' => $PersonsStudentsGet[72],
                'IsAfterDiplomEducation' => $PersonsStudentsGet[73],
                'BudjetYear' => $PersonsStudentsGet[74],
                'IsProgramDoubleDiploms' => $PersonsStudentsGet[75],
                ))->execute();
    }
    //echo $SessionGUID;
    //var_dump($PersonsStudentsGet);

    return "+";
}


function _edboPersonDocumentsAdd($SessionGUID, $file)
{
    global $soapClient;
    global $soap;
    //$SessionGUID="ea30eeb6-b94e-47af-b38a-cd6a4bdcf6c8";
    $path = '/var/www/sites/default/files/passport/' . $file;
    $fp = fopen('/var/www/sites/default/files/passport/log2.txt', 'a+');
    module_load_include('inc', 'phpexcel');
    $result = phpexcel_import($path);
    //print_r($result);
    // print $SessionGUID; exit;
    foreach ($result[0] as $person_doc) {
        //  print_r($person_doc);
        $PersonsStudentsGet = $soap->invoke("PersonDocumentsAdd", array(
            "SessionGUID" => $SessionGUID,
            "Id_Language" => 1,
            "Id_Person" => $person_doc['id'],
            "Id_PersonDocumentType" => $person_doc['type'],
            "Id_PersonEducation" => 0,
            "DocumentSeries" => $person_doc['se'],
            "DocumentNumbers" => $person_doc['no'],
            "DocumentDateGet" => $person_doc['data'],
            "DocumentIssued" => $person_doc['issued'],
            "Description" => "",
            "ZNOPin" => 0,
            "AttestatValue" => "",
            "IsCheckForPaperCopy" => 1,
            "Id_PersonDocumentsAwardType" => 0));
        fwrite($fp, $person_doc['id'] . " >> " . $PersonsStudentsGet . "\r\n");
        echo $person_doc['id'] . ">>" . $PersonsStudentsGet . "<br />";
    }

    $error = $soap->invoke("GetLastError", array("SessionGUID" => $SessionGUID));
    //var_dump($PersonsStudentsGet);
    print_r($error); //echo  $PersonsStudentsGet;
    //
    fclose($fp);
    exit;
    //return $PersonsStudentsGet;
}

function _edboPersonDocumentsDel($SessionGUID, $file)
{
    global $soapClient;
    global $soap;
    $path = 'files/passport/' . $file;
    $fp = fopen('files/passport/del.log.txt', 'a+');
    module_load_include('inc', 'phpexcel');
    $result = phpexcel_import($path);
    print_r($result);
    foreach ($result[0] as $person_doc) {
        //  print_r($person_doc);
        $PersonsStudentsGet = $soap->invoke("PersonDocumentsDel", array(
            "SessionGUID" => $SessionGUID,


            "Id_PersonDocument" => $person_doc['nd'],

            ));
        fwrite($fp, $person_doc['nd'] . " >> " . $PersonsStudentsGet . "\r\n");
        echo $person_doc['nd'] . ">>" . $PersonsStudentsGet . "<br />";
    }

    $error = $soap->invoke("GetLastError", array("SessionGUID" => $SessionGUID));
    //var_dump($PersonsStudentsGet);
    print_r($error); //echo  $PersonsStudentsGet;
    //
    fclose($fp);
    exit;
    //return $PersonsStudentsGet;
}


//SELECT n.Cource,n.PIB,n.N,b.PersonCodeU FROM `5_1427500483_contingent` n inner join PersonsStudentsGet b on n.N=b.Id_Person WHERE n.end_edu='01.01.0001 00:00:00'  AND n.Cource='2 Курс'
function _edboPersonsEducationsDatesEndSet($SessionGUID)
{
    global $soapClient;
    global $soap;
    set_time_limit(0);
    $nodes = db_select('PersonEducationsGet2', 'n')->fields('n', array(
        'Id_PersonEducation',
        'CourseName',
        'PersonEducationFormName',
        'Id_University')) //  ->condition('n.PersonEducationDateEnd', '0001-01-01 00:00:00')
        ->condition('n.PersonEducationDateEnd', '2015-01-30 00:00:00')->condition(db_or
        ()->condition('Id_University', 341)->condition('Id_University', 2712)->
        condition('Id_University', 2736)->condition('Id_University', 2738)->condition('Id_University',
        2739)->condition('Id_University', 2740))->execute();
    foreach ($nodes as $nodesql) {
        switch ($nodesql->Id_University) {
            case 341:
                $Ukode = "ac189d29-eec1-4aaf-8ace-d952dff91344";
                break;
            case 2712:
                $Ukode = "62c34f4d-0a7c-4e69-9eb2-522c069121d1";
                break;
            case 2736:
                $Ukode = "6782958c-8b1b-4633-bc4b-eb45cef3fdf5";
                break;
            case 2738:
                $Ukode = "99a1817e-7a91-4cc0-b931-b410b732eb69";
                break;
            case 2739:
                $Ukode = "bf343633-253a-46c8-9017-9ea65eab0dd4";
                break;
            case 2740:
                $Ukode = "bd9a3c6b-0795-41d9-9432-04b3e60ac7a6";
                break;
            default:
                continue;
        }
        if ($nodesql->PersonEducationFormName == 'Денна' && $nodesql->CourseName ==
            '4 Курс') {
            $enddate = "30.06.2015 00:00:00";
        } else
            if ($nodesql->PersonEducationFormName == 'Заочна' && $nodesql->CourseName ==
                '4 Курс') {
                $enddate = "01.07.2015 00:00:00";
            } else
                if ($nodesql->PersonEducationFormName == 'Заочна' && $nodesql->CourseName ==
                    '3 Курс') {
                    $enddate = "01.07.2016 00:00:00";
                } else
                    if ($nodesql->PersonEducationFormName == 'Заочна' && $nodesql->CourseName ==
                        '2 Курс') {
                        $enddate = "01.07.2017 00:00:00";
                    } else
                        if ($nodesql->PersonEducationFormName == 'Денна' && $nodesql->CourseName ==
                            '3 Курс') {
                            $enddate = "30.06.2016 00:00:00";
                        } else
                            if ($nodesql->PersonEducationFormName == 'Денна' && $nodesql->CourseName ==
                                '2 Курс') {
                                $enddate = "30.06.2017 00:00:00";
                            } else
                                continue;
        $PersonsStudentsGet = $soap->invoke("PersonEducationsStatusChangeDates", array(
            "SessionGUID" => $SessionGUID,
            "UniversityKode" => $Ukode,
            "PersonEducationDateEnd" => $enddate,
            "Id_PersonEducations" => "~1#" . $nodesql->Id_PersonEducation,

            ));
        return;
    }

    return;
}
